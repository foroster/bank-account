from ..base.exception import ApplicationError
import json
from ..base.response import ResponseSuccess, ResponseFailure
from typing import Union
import logging


class ResponseEncoder(object):

    def default(self, o: Union['ResponseFailure', 'ResponseSuccess'])->dict:
        try:
            response = {
                'meta': None,
                'data': None,
                'error': [],
            }
            if not bool(o):
                for error in o.error_list:
                    response['error'].append({
                        'message': getattr(error, 'message', str(error)),
                        'code': getattr(error, 'code'),
                        'data': getattr(error, 'data', [])
                    })
            else:
                response['data'] = o.value
            return json.dumps(response)
        except Exception as e:
            logging.getLogger(__file__).error('Encoder error:{}'.format(str(e)))
            return json.dumps({
                'meta': None,
                'data': None,
                'error': [{
                    'message':ApplicationError.message,
                    'code': ApplicationError.code,
                    'data': ApplicationError.data
                }],
            })
