from .exception import ApplicationError
from .request import InvalidRequest
import typing
import logging


class BaseResponse(object):
    pass

class ResponseSuccess(BaseResponse):

    def __init__(self, value: object=None):
        self.value = value

    def __nonzero__(self) -> bool:
        return True

    __bool__ = __nonzero__


class ResponseFailure(BaseResponse):

    def __init__(self, error_list: typing.List[ApplicationError]):
        self.error_list = error_list

    def __bool__(self) -> bool:
        return False

    def get_display_error_message(self, *args, **kwargs) -> dict:
        errors = []
        for error in self.error_list:
            errors.append({
                'message': getattr(error, 'message', str(error)),
                'code': getattr(error, 'code'),
                'data': getattr(error, 'data', [])
            })
        return {
            "error": errors
        }

    @classmethod
    def build_from_exception(cls, exception: Exception):
        if not issubclass(type(exception), ApplicationError):
            exception = ApplicationError(message=str(exception))
        return cls([exception])

    @classmethod
    def build_from_invalid_request(cls, invalid_request: 'InvalidRequest')-> 'ResponseFailure':
        return cls(invalid_request.errors)
