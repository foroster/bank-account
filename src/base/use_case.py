import logging
from typing import Union
from .request import ValidRequest
from .response import ResponseFailure, ResponseSuccess


class BaseUseCase(object):

    def execute(self, request: ValidRequest, *args, **kwargs) -> Union[ResponseSuccess, ResponseFailure]:
        try:
            if request.request_is_valid():
                return self.__execute__(request)
        except Exception as e:
            logging.getLogger(__file__).error('UseCase execte error:{}'.format(str(e)))
            return ResponseFailure.build_from_exception(e)

    def __execute__(self, request: ValidRequest, *args, **kwargs)-> Union[ResponseSuccess, ResponseFailure]:
        raise NotImplementedError()
