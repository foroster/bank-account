class ApplicationError(Exception):
    code = '9a4f08d2-72cd-4d6c-9592-f5d4d14eb8cb'
    message = 'An error occured'
    data = None

    def __init__(self, message: str = None, data = None):
        if message:
            self.message = message
        if data:
            self.data = data


class InvalidRequestError(ApplicationError):
    code = '7815550e-5288-411d-b66f-537e3ed3b867'
    message = 'Invalid request'
    data = None

class AdapterError(ApplicationError):
    code = '2d6e832a-cc8e-4d24-9c69-fbeb2c1de70d'
    message = 'Adapter error'
    data = None
