import typing


class BaseRequest(object):
    pass


class InvalidRequest(BaseRequest):

    def __init__(self):
        self.errors = []

    def add_error(self, error: Exception)->None:
        self.errors.append(error)

    def has_errors(self)->bool:
        return len(self.errors) > 0

    def __nonzero__(self)->bool:
        return False

    __bool__ = __nonzero__


class ValidRequest(BaseRequest):

    def request_is_valid(self, *args, **kwargs)-> 'typing.Union[ValidRequest, InvalidRequest]':
        raise NotImplementedError()

    @classmethod
    def from_dict(cls, adict:dict):
        raise NotImplementedError()

    def __nonzero__(self)->bool:
        return True

    __bool__ = __nonzero__
