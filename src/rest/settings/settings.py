import os


PROJECT_ROOT = os.path.abspath(os.path.join(__file__, '../../../../'))
SRC_ROOT = os.path.abspath(os.path.join(PROJECT_ROOT, 'src'))
STORAGE_CONNECTION_STRING = "postgresql://bank_account:bank_account@localhost/bank_account"
WS_PREFXI = 'ws'
HOST = '127.0.0.1'
PORT = 5000

ANTI_MONEY_LAUNDERING_DAY = 5
ANTI_MONEY_LAUNDERING_AMOUNT = 10000
ANTI_MONEY_LAUNDERING_CURRENCY = 'EUR'

try:
    from .local_settings import *
except ImportError:
    pass
