import sys
import os
sys.path.append(os.path.abspath(os.path.join(__file__, '../../../../../')))

from tornado import websocket, web, ioloop
import json
import logging
from src.serializers.response_serializer import ResponseEncoder
from src.base.response import ResponseFailure
from src.base.exception import InvalidRequestError
from src.libs.facade import Facade
from src.rest.settings import settings


class SocketHandler(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        logging.getLogger(__file__).debug("WebSocket opened")

    def on_message(self, message):
        facade = Facade()
        try:
            response = facade.dispath(json.loads(message))
        except Exception as e:
            logging.getLogger(__file__).error('SocketHandler error:{}'.format(str(e)))
            response = ResponseFailure.build_from_exception(InvalidRequestError())
        self.write_message(ResponseEncoder().default(response))

    def on_close(self):
        logging.debug("WebSocket closed")


app = web.Application([
    (r'/{}'.format(settings.WS_PREFXI), SocketHandler),
])

if __name__ == '__main__':
    app.listen(settings.PORT, address=settings.HOST)
    ioloop.IOLoop.instance().start()
