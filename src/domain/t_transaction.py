from .base import Base
from sqlalchemy import Column, Integer, Float, ForeignKey, String, text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship


class TTransaction(Base):
    __tablename__ = 't_transaction'

    t_transaction_id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v1()"))
    timestamp = Column(Integer, nullable=False, server_default=text("0"))
    t_account_id = Column(ForeignKey('t_account.t_account_id'), nullable=False)
    t_currency_id = Column(ForeignKey('t_currency.t_currency_id'), nullable=False)
    currency_value = Column(Float(53), nullable=False)
    base_currency_value = Column(Float(53), nullable=True, server_default=text("0"))
    type = Column(String(50), nullable=False)

    t_account = relationship('TAccount')
    t_currency = relationship('TCurrency')
