from .base import Base
from sqlalchemy import Column, DateTime, Float, ForeignKey, String, text
from sqlalchemy.dialects.postgresql import UUID


class TAccount(Base):
    __tablename__ = 't_account'

    t_account_id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v1()"))
    name = Column(String(50), nullable=False, unique=True)
