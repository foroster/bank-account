from .base import Base
from sqlalchemy import Column, String, text
from sqlalchemy.dialects.postgresql import UUID


class TCurrency(Base):
    __tablename__ = 't_currency'

    t_currency_id = Column(UUID, primary_key=True, unique=True, server_default=text("uuid_generate_v1()"))
    name = Column(String(3), nullable=False, unique=True)
