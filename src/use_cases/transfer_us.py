from src.base.use_case import BaseUseCase
from src.base.request import ValidRequest, InvalidRequest
from jsonschema import validate
from src.base.exception import InvalidRequestError
from src.base.response import ResponseSuccess, ResponseFailure
from src.libs.storage import get_session
from src.domain.t_account import TAccount
from src.domain.t_currency import TCurrency
from src.domain.t_transaction import TTransaction
from .exception import *
from datetime import datetime, timedelta
import logging
from sqlalchemy import func
from src.rest.settings import settings
from src.adapter.open_rates import OpenRates

class TransferValidRequest(ValidRequest):

    schema = {
        "type": "object",
        "properties": {
            "method": {
                "type": "string",
                "enum": ["transfer"]
            },
            "from_account": {"type": "string"},
            "to_account": {"type": "string"},
            "amt": {"type": "number"},
            "ccy": {"type": "string"},
        }
    }

    def __init__(self, data: dict):
        self.data = data

    def request_is_valid(self, *args, **kwargs):
        try:
            validate(self.data, self.schema)
        except Exception as ex:
            e = InvalidRequestError()
            e.data = str(ex)
            request = InvalidRequest()
            request.add_error(e)
            return request
        return self


class TransferUseCase(BaseUseCase):

    def __init__(self, rates_adapter: OpenRates):
        super().__init__()
        self.rates_adapter = rates_adapter

    def __execute__(self, request: TransferValidRequest, *args, **kwargs):
        session = get_session()

        from_account = session.query(TAccount).filter(TAccount.name.in_([request.data['from_account']])).first()
        if not from_account:
            return ResponseFailure.build_from_exception(AccountNotFoundError(data=request.data['from_account']))
        to_account = session.query(TAccount).filter(TAccount.name.in_([request.data['to_account']])).first()
        if not to_account:
            return ResponseFailure.build_from_exception(AccountNotFoundError(data=request.data['to_account']))
        currency = session.query(TCurrency).filter(TCurrency.name.in_([request.data['ccy']])).first()
        if not currency:
            return ResponseFailure.build_from_exception(CurrencyNotFoundError())
        amount = request.data['amt']
        if amount <= 0:
            return ResponseFailure.build_from_exception(WrongAmountError())

        from_account_balance = session.query(
            func.sum(TTransaction.currency_value).label('balance')
        ).filter(
            TTransaction.t_currency_id == currency.t_currency_id,
            TTransaction.t_account_id == from_account.t_account_id
        ).group_by(
            TTransaction.t_account_id,
            TTransaction.t_currency_id
        ).first()
        if from_account_balance:
            from_account_balance = float(from_account_balance[0])
        else:
            return ResponseFailure.build_from_exception(NotEnoughFundsError())

        if amount > from_account_balance:
            return ResponseFailure.build_from_exception(NotEnoughFundsError(
                data="{} balance is {}".format(currency.name, str(from_account_balance))
            ))
        elif amount > settings.ANTI_MONEY_LAUNDERING_AMOUNT:
            return ResponseFailure.build_from_exception(WrongAmountError(
                data="The amount should be less than {} {}".format(
                    str(settings.ANTI_MONEY_LAUNDERING_AMOUNT),
                    str(settings.ANTI_MONEY_LAUNDERING_CURRENCY),
                )
            ))

        if currency.name != settings.ANTI_MONEY_LAUNDERING_CURRENCY:
            base_currency_value = self.rates_adapter.convert(
                currency.name,
                settings.ANTI_MONEY_LAUNDERING_CURRENCY,
                amount
            )
        else:
            base_currency_value = amount

        total_transfer_amount = session.query(
            func.sum(TTransaction.base_currency_value).label('total_transfer_amount')
        ).filter(
            TTransaction.t_account_id == from_account.t_account_id,
            TTransaction.type == request.data['method'],
            TTransaction.timestamp > int((datetime.utcnow() - timedelta(days=settings.ANTI_MONEY_LAUNDERING_DAY)).timestamp()),
            TTransaction.base_currency_value < 0    # only outgoing transfers
        ).group_by(
            TTransaction.t_account_id,
            TTransaction.t_currency_id
        ).all()

        if total_transfer_amount:
            # sum transfers in all currencies
            tmp = 0
            for row in total_transfer_amount:
                tmp += float(row[0])
            total_transfer_amount = tmp * -1

            if total_transfer_amount + base_currency_value > float(settings.ANTI_MONEY_LAUNDERING_AMOUNT):
                return ResponseFailure.build_from_exception(AntiMoneyLaunderingError(
                    data="{} already transferred {} {} for last {} days".format(
                        from_account.name,
                        str(total_transfer_amount),
                        str(settings.ANTI_MONEY_LAUNDERING_CURRENCY),
                        str(settings.ANTI_MONEY_LAUNDERING_DAY)
                    )
                ))

        try:
            session.add(TTransaction(
                timestamp=int(datetime.utcnow().timestamp()),
                t_account=from_account,
                t_currency=currency,
                currency_value=-amount,
                type=request.data['method'],
                base_currency_value=-base_currency_value
            ))
            session.add(TTransaction(
                timestamp=int(datetime.utcnow().timestamp()),
                t_account=to_account,
                t_currency=currency,
                currency_value=amount,
                type=request.data['method'],
                base_currency_value=base_currency_value
            ))
            session.commit()
        except Exception as e:
            try:
                session.rollback()
            except Exception as ex:
                logging.getLogger(__file__).error('WithdrawalUseCase rollback error: {}'.format(str(ex)))
            logging.getLogger(__file__).error('WithdrawalUseCase save error: {}'.format(str(e)))
            return ResponseFailure.build_from_exception(ApplicationError(data=str(e)))
        return ResponseSuccess(value={'message': '{} transfered {} {} to {}'.format(
            from_account.name, str(amount), currency.name, to_account.name
        )})

