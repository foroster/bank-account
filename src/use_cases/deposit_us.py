from src.base.use_case import BaseUseCase
from src.base.request import ValidRequest, InvalidRequest
from jsonschema import validate
from src.base.exception import InvalidRequestError
from src.base.response import ResponseSuccess, ResponseFailure
from src.libs.storage import get_session
from src.domain.t_account import TAccount
from src.domain.t_currency import TCurrency
from src.domain.t_transaction import TTransaction
from .exception import *
from datetime import datetime
import logging

class DepositValidRequest(ValidRequest):

    schema = {
        "type": "object",
        "properties": {
            "method": {
                "type": "string",
                "enum": ["deposit"]
            },
            "account": {"type": "string"},
            "amt": {"type": "number"},
            "ccy": {"type": "string"},
        }
    }

    def __init__(self, data: dict):
        self.data = data

    def request_is_valid(self, *args, **kwargs):
        try:
            validate(self.data, self.schema)
        except Exception as ex:
            e = InvalidRequestError()
            e.data = str(ex)
            request = InvalidRequest()
            request.add_error(e)
            return request
        return self


class DepositUseCase(BaseUseCase):

    def __execute__(self, request: DepositValidRequest, *args, **kwargs):
        session = get_session()

        account = session.query(TAccount).filter(TAccount.name.in_([request.data['account']])).first()
        if not account:
            return ResponseFailure.build_from_exception(AccountNotFoundError())
        currency = session.query(TCurrency).filter(TCurrency.name.in_([request.data['ccy']])).first()
        if not currency:
            return ResponseFailure.build_from_exception(CurrencyNotFoundError())
        amount = request.data['amt']
        if amount <= 0:
            return ResponseFailure.build_from_exception(WrongAmountError())

        transaction = TTransaction(
            timestamp=int(datetime.utcnow().timestamp()),
            t_account=account,
            t_currency=currency,
            currency_value=amount,
            type=request.data['method']
        )
        try:
            session.add(transaction)
            session.commit()
        except Exception as e:
            logging.getLogger(__file__).error('DepositUseCase save error: {}'.format(str(e)))
            return ResponseFailure.build_from_exception(ApplicationError(data=str(e)))
        return ResponseSuccess(value={'message': '{} deposit {} {}'.format(
            account.name, str(amount), currency.name
        )})

