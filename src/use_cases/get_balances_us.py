from src.base.use_case import BaseUseCase
from src.base.request import ValidRequest, InvalidRequest
from jsonschema import validate
from src.base.exception import InvalidRequestError
from src.base.response import ResponseSuccess, ResponseFailure
from src.libs.storage import get_session
from src.domain.t_account import TAccount
from src.domain.t_currency import TCurrency
from src.domain.t_transaction import TTransaction
from .exception import *
from datetime import datetime, timedelta
import logging
from sqlalchemy import func
from src.rest.settings import settings
from src.adapter.open_rates import OpenRates


class GetBalancesValidRequest(ValidRequest):

    schema = {
        "type": "object",
        "properties": {
            "method": {
                "type": "string",
                "enum": ["get_balances"]
            },
            "date": {"type": "string"},
            "account": {"type": "string"},
        }
    }

    def __init__(self, data: dict):
        self.data = data

    def request_is_valid(self, *args, **kwargs):
        try:
            validate(self.data, self.schema)
        except Exception as ex:
            e = InvalidRequestError()
            e.data = str(ex)
            request = InvalidRequest()
            request.add_error(e)
            return request
        return self


class GetBalancesUseCase(BaseUseCase):

    def __execute__(self, request: GetBalancesValidRequest, *args, **kwargs):
        session = get_session()

        account = session.query(TAccount).filter(TAccount.name.in_([request.data['account']])).first()
        if not account:
            return ResponseFailure.build_from_exception(AccountNotFoundError())
        try:
            timestamp = int(datetime.strptime(request.data['date'], '%Y-%m-%d').timestamp())
        except Exception as e:
            return ResponseFailure.build_from_exception(InvalidRequestError(
                data='{} is invalid date'.format(request.data['date']))
            )

        balance = session.query(
            func.sum(TTransaction.currency_value).label('balance'),
            TCurrency.name
        ).join(
            TCurrency
        ).filter(
            TTransaction.t_account_id == account.t_account_id,
            TTransaction.timestamp <= timestamp,

        ).group_by(
            TTransaction.t_account_id,
            TCurrency.name
        ).all()

        response = {
            'date': request.data['date'],
            'balances':{}
        }
        for row in balance:
            response['balances'].update({row[1]: row[0]})

        return ResponseSuccess(value=response)

