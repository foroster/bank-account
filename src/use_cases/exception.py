from ..base.exception import ApplicationError


class AccountNotFoundError(ApplicationError):
    code = 'f84b2c98-4c67-4c99-8979-619422666f15'
    message = 'Account not found'
    data = None


class CurrencyNotFoundError(ApplicationError):
    code = 'ee986f33-1182-4e24-87f9-573e86ed8a57'
    message = 'Currency not found'
    data = None


class WrongAmountError(ApplicationError):
    code = 'ee986f33-1182-4e24-87f9-573e86ed8a57'
    message = 'Wrong amount'
    data = None


class NotEnoughFundsError(ApplicationError):
    code = '7b62b200-b829-4876-b560-264a9b4e6766'
    message = 'Not enough funds'
    data = None


class AntiMoneyLaunderingError(ApplicationError):
    code = '7b62b200-b829-4876-b560-264a9b4e6766'
    message = 'Anti money laundering error'
    data = None
