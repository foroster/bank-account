from typing import Union
from src.base.response import ResponseFailure, ResponseSuccess
from src.base.exception import InvalidRequestError
from src.use_cases.deposit_us import DepositUseCase, DepositValidRequest
from src.use_cases.get_balances_us import GetBalancesUseCase, GetBalancesValidRequest
from src.use_cases.transfer_us import TransferUseCase, TransferValidRequest
from src.use_cases.withdrawal_us import WithdrawalUseCase, WithdrawalValidRequest
from src.adapter.open_rates import OpenRates


class Facade(object):

    def dispath(self, data: dict, *args, **kwargs) -> Union[ResponseSuccess, ResponseFailure]:
        map = {
            'deposit': self.deposit,
            'get_balances': self.get_balances,
            'transfer': self.transfer,
            'withdrawal': self.withdrawal
        }
        func = map.get(data.get('method', None), None)
        if not func:
            return ResponseFailure.build_from_exception(InvalidRequestError())
        else:
            try:
                return func(data, args, kwargs)
            except Exception as e:
                ResponseFailure.build_from_exception(e)

    def deposit(self, data:dict, *args, **kwargs)-> Union[ResponseSuccess, ResponseFailure]:
        return DepositUseCase().execute(request=DepositValidRequest(data=data))

    def get_balances(self, data:dict , *args, **kwargs)-> Union[ResponseSuccess, ResponseFailure]:
        return GetBalancesUseCase().execute(request=GetBalancesValidRequest(data=data))

    def transfer(self, data:dict, *args, **kwargs)-> Union[ResponseSuccess, ResponseFailure]:
        return TransferUseCase(rates_adapter=OpenRates).execute(request=TransferValidRequest(data=data))

    def withdrawal(self, data:dict, *args, **kwargs)-> Union[ResponseSuccess, ResponseFailure]:
        return WithdrawalUseCase().execute(request=WithdrawalValidRequest(data=data))
