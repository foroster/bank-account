from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session
from src.rest.settings import settings


def get_session(*args, **kwargs) -> Session:
    engine = create_engine(
        settings.STORAGE_CONNECTION_STRING,
        isolation_level="READ UNCOMMITTED"
    )
    Session = sessionmaker(bind=engine)
    session = Session()
    return session
