from src.base.adapter import BaseAdapter
from src.base.exception import AdapterError
import requests
import logging


class OpenRates(BaseAdapter):

    host = 'http://api.openrates.io/'

    @classmethod
    def convert(cls, from_ticket:str, to_ticket: str, from_ticket_value: float) -> float:
        from_ticket = from_ticket.upper()
        to_ticket = to_ticket.upper()
        url = '{}latest?base={}'.format(cls.host, from_ticket)
        try:
            response = requests.get(url).json()
            price_per_from = response.get('rates', {}).get(to_ticket, -1)
            if price_per_from > 0:
                return price_per_from * from_ticket_value
        except Exception as e:
            logging.getLogger(__file__).error('OpenRates error: {}'.format(str(e)))
            raise AdapterError(data=str(e))
