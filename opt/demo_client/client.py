import asyncio
import websockets
import json


async def send():
    async with websockets.connect(
            'ws://206.189.2.16:80/ws') as websocket:

        # message = {
        #     'method': 'deposit',
        #     # 'method': 'withdrawal',
        #     'account': 'bob',
        #     'amt': 100000,
        #     'ccy': 'EUR'
        # }
        message = {
            'method': 'get_balances',
            'date': '2019-12-01',
            'account': 'bob'
        }
        #
        # message = {
        #     'method': 'transfer',
        #     'from_account': 'bob',
        #     'to_account': 'alice',
        #     'amt': 1200,
        #     'ccy': 'EUR'
        # }
        await websocket.send(json.dumps(message))
        print(f"> {message}")

        greeting = await websocket.recv()
        print(f"< {greeting}")

asyncio.get_event_loop().run_until_complete(send())