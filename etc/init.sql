-- SCHEMA

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create table t_account
(
	t_account_id uuid default uuid_generate_v1() not null,
	name varchar(50) not null
);

create unique index t_account_name_uindex
	on t_account (name);

create unique index t_account_t_account_id_uindex
	on t_account (t_account_id);

alter table t_account
	add constraint t_account_pk
		primary key (t_account_id);

create table t_currency
(
	t_currency_id uuid default uuid_generate_v1() not null,
	name varchar(3) not null
);

create unique index t_currency_name_uindex
	on t_currency (name);

create unique index t_currency_t_currency_id_uindex
	on t_currency (t_currency_id);

alter table t_currency
	add constraint t_currency_pk
		primary key (t_currency_id);

create table t_transaction
(
	t_transaction_id uuid default uuid_generate_v1() not null,
    timestamp int default  0 not null,
	t_account_id uuid not null
		constraint t_transaction_t_account_t_account_id_fk
			references t_account,
	t_currency_id uuid not null
		constraint t_transaction_t_currency_t_currency_id_fk
			references t_currency,
	currency_value float not null,
	base_currency_value float,
	type varchar(50) not null
);

create unique index t_transaction_t_transaction_id_uindex
	on t_transaction (t_transaction_id);

alter table t_transaction
	add constraint t_transaction_pk
		primary key (t_transaction_id);


-- INIT DATA

insert into t_account(t_account_id, name) values (uuid_generate_v1(), 'bob');
insert into t_account(t_account_id, name) values (uuid_generate_v1(), 'alice');

insert into t_currency(t_currency_id, name) values (uuid_generate_v1(), 'USD');
insert into t_currency(t_currency_id, name) values (uuid_generate_v1(), 'GBP');
insert into t_currency(t_currency_id, name) values (uuid_generate_v1(), 'EUR');
insert into t_currency(t_currency_id, name) values (uuid_generate_v1(), 'JPY');
insert into t_currency(t_currency_id, name) values (uuid_generate_v1(), 'RUB');


insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'bob') as t_account_id,
  (select t_currency_id from t_currency where name = 'USD'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'bob') as t_account_id,
  (select t_currency_id from t_currency where name = 'GBP'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'bob') as t_account_id,
  (select t_currency_id from t_currency where name = 'EUR'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'bob') as t_account_id,
  (select t_currency_id from t_currency where name = 'JPY'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'bob') as t_account_id,
  (select t_currency_id from t_currency where name = 'RUB'),
  0,
  'init';


insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'alice') as t_account_id,
  (select t_currency_id from t_currency where name = 'USD'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'alice') as t_account_id,
  (select t_currency_id from t_currency where name = 'GBP'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'alice') as t_account_id,
  (select t_currency_id from t_currency where name = 'EUR'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'alice') as t_account_id,
  (select t_currency_id from t_currency where name = 'JPY'),
  0,
  'init';

insert into t_transaction (t_account_id, t_currency_id, currency_value, type)
select
  (select t_account_id from t_account where name = 'alice') as t_account_id,
  (select t_currency_id from t_currency where name = 'RUB'),
  0,
  'init';